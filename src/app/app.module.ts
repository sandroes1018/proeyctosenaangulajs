import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DataTablesModule } from 'angular-datatables';
import { FullCalendarModule } from 'ng-fullcalendar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { StorageServiceModule} from 'angular-webstorage-service';
/*rutas*/
import { appRoutes } from './routes/route.module';

/*Serviicios*/
import { MenuService } from './admin//adminComponent/menu-header/serviceMenu';
import { SuscripcionService } from './admin/adminComponent/servicios/suscripcion-service';
import { TipoServicioService } from './admin/adminComponent/servicios/tipos-servicios-service';


/**modulos */
import { AppComponent } from './app.component';
/** componentesa admins */
import { MenuHeaderComponent } from './admin/adminComponent/menu-header/menu-header.component';
import { FooterComponent } from './admin/adminComponent/footer/footer.component';
import { TemplateAdminComponent } from './admin/adminComponent/template-admin/template-admin.component';
import { InicioAdminComponent } from './admin/inicio-admin/inicio-admin.component';
import { ServiciosComponent } from './admin/controlServicios/servicios.component';
import { CarteraComponent } from './admin/cartera/cartera.component';
import { AgendamientosComponent } from './admin/agendamientos/agendamientos.component';
import { ClienteComponent } from './cliente/cliente.component';
import { IndexComponent } from './index/index.component';
import { ErrorNotFoundComponent } from './error/404/error-not-found/error-not-found.component';
import { ErrorServerComponent } from './error/500/error-server/error-server.component';
import { ConsultarfacturaComponent } from './admin/cartera/consultarfactura/consultarfactura.component';
import { GenerarFacturaComponent } from './admin/cartera/generar-factura/generar-factura.component';
import { GenerarPagoComponent } from './admin/cartera/generar-pago/generar-pago.component';
import { ProgramarActividadCorteComponent } from './admin/cartera/programar-actividad-corte/programar-actividad-corte.component';
import { ReportarCorteComponent } from './admin/cartera/reportar-corte/reportar-corte.component';
import { AutorizarServicioComponent } from './admin/controlServicios/autorizar-servicio/autorizar-servicio.component';
import { DetalleAutorizarServicioComponent } from './admin/controlServicios/detalleAutorizarServicio/detalle-autorizar-servicio.component';
import { GenerarProgramamcionDiariaComponent } from './admin/controlServicios/generarProgDiaria/generar-programamcion-diaria.component';
import { GenerarServicioActividadComponent } from './admin/controlServicios/generarServicioActividad/generar-servicio-actividad.component';
import { GenerarServicioExtraComponent } from './admin/controlServicios/generar-servicio-extra/generar-servicio-extra.component';
import { GenerarServicioComponent } from './admin/controlServicios/generar-servicio/generar-servicio.component';
import { GenerarSuscripcionComponent } from './admin/controlServicios/generar-suscripcion/generar-suscripcion.component';
import { HistorialAgendamientosComponent } from './admin/agendamientos/historial-agendamientos/historial-agendamientos.component';
import { RegistrarMaterialComponent } from './admin/agendamientos/registrar-material/registrar-material.component';
import { ServiciosAgendadosComponent } from './admin/agendamientos/servicios-agendados/servicios-agendados.component';
import { ClientesMoraComponent } from './admin/cartera/clientes-mora/clientes-mora.component';
import { TiposServiciosComponent } from './admin/controlServicios/tipos-servicios/tipos-servicios.component';
import { PersonaService } from './admin/adminComponent/servicios/persona-service';
import { LoginRegistroComponent } from './index/login-registro/login-registro.component';
import { LoginService } from './admin/adminComponent/servicios/login-service';
import { FacturaService } from './admin/adminComponent/servicios/factura-service';
import { PagoService } from './admin/adminComponent/servicios/pago-service';
import { ProgActividadServicioComponent } from './admin/agendamientos/progActividadServicio/prog-actividad-servicio.component';
import { CronogramaComponent } from './admin/agendamientos/cronograma-agendar/cronograma.component';
import { UsuariosComponent } from './admin/usuarios/usuarios.component';
import { UsuarioService } from './admin/adminComponent/servicios/usuario-service';
import { RolesService } from './admin/adminComponent/servicios/roles-service';
import { AuthGuard } from './admin/adminComponent/servicios/authGuard ';
import { EditarDatosComponent } from './admin/usuarios/editar-datos/editar-datos.component';
import { MigracionComponent } from './admin/migracion/migracion/migracion.component';

@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    IndexComponent,
    /** componentesa admins */
    MenuHeaderComponent,
    FooterComponent,
    TemplateAdminComponent,
    InicioAdminComponent,
    /**paginas error */
    ErrorNotFoundComponent,
    ErrorServerComponent,
    /**cartera ***************************/
    CarteraComponent,
    ConsultarfacturaComponent,
    GenerarFacturaComponent,
    GenerarPagoComponent,
    ProgramarActividadCorteComponent,
    ReportarCorteComponent,
    ClientesMoraComponent,
    /**control servicios ****************/
    ServiciosComponent,
    AutorizarServicioComponent,
    DetalleAutorizarServicioComponent,
    GenerarProgramamcionDiariaComponent,
    GenerarServicioActividadComponent,
    GenerarServicioExtraComponent,
    GenerarServicioComponent,
    GenerarSuscripcionComponent,
    ProgActividadServicioComponent,
    TiposServiciosComponent,
    /**agendamientos ******************/
    AgendamientosComponent,
    CronogramaComponent,
    HistorialAgendamientosComponent,
    RegistrarMaterialComponent,
    ServiciosAgendadosComponent,
    LoginRegistroComponent,
    UsuariosComponent,
    EditarDatosComponent,
    MigracionComponent,
  ],
  imports: [
    StorageServiceModule,
    NgBootstrapFormValidationModule.forRoot(),
    NgBootstrapFormValidationModule,
    SweetAlert2Module.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserModule,
    DataTablesModule,
    FullCalendarModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthGuard,
    RolesService,
    UsuarioService,
    PagoService,
    FacturaService,
    LoginService,
    PersonaService,
    SuscripcionService,
    TipoServicioService,
    MenuService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
