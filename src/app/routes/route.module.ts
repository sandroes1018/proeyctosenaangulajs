import { Routes, ChildrenOutletContexts } from '@angular/router';

import { ServiciosComponent } from '../admin/controlServicios/servicios.component';
import { CarteraComponent } from '../admin/cartera/cartera.component';
import { AgendamientosComponent } from '../admin/agendamientos/agendamientos.component';
import { InicioAdminComponent } from '../admin/inicio-admin/inicio-admin.component';
import { GenerarSuscripcionComponent } from 'src/app/admin/controlServicios/generar-suscripcion/generar-suscripcion.component';
import { GenerarServicioComponent } from 'src/app/admin/controlServicios/generar-servicio/generar-servicio.component';
import { AutorizarServicioComponent } from '../admin/controlServicios/autorizar-servicio/autorizar-servicio.component';
import { GenerarProgramamcionDiariaComponent } from '../admin/controlServicios/generarProgDiaria/generar-programamcion-diaria.component';
import { GenerarServicioExtraComponent } from '../admin/controlServicios/generar-servicio-extra/generar-servicio-extra.component';
import { GenerarServicioActividadComponent } from '../admin/controlServicios/generarServicioActividad/generar-servicio-actividad.component';
import { TiposServiciosComponent } from '../admin/controlServicios/tipos-servicios/tipos-servicios.component';
import { ErrorNotFoundComponent } from '../error/404/error-not-found/error-not-found.component';
import { IndexComponent } from '../index/index.component';
import { TemplateAdminComponent } from '../admin/adminComponent/template-admin/template-admin.component';
import { ClientesMoraComponent } from '../admin/cartera/clientes-mora/clientes-mora.component';
import { ConsultarfacturaComponent } from '../admin/cartera/consultarfactura/consultarfactura.component';
import { GenerarFacturaComponent } from '../admin/cartera/generar-factura/generar-factura.component';
import { GenerarPagoComponent } from '../admin/cartera/generar-pago/generar-pago.component';
/* control agendamientos  -----*/
import { ProgramarActividadCorteComponent } from '../admin/cartera/programar-actividad-corte/programar-actividad-corte.component';
import { ReportarCorteComponent } from '../admin/cartera/reportar-corte/reportar-corte.component';
import { RegistrarMaterialComponent } from '../admin/agendamientos/registrar-material/registrar-material.component';
import { ServiciosAgendadosComponent } from '../admin/agendamientos/servicios-agendados/servicios-agendados.component';
import { HistorialAgendamientosComponent } from '../admin/agendamientos/historial-agendamientos/historial-agendamientos.component';
import { ProgActividadServicioComponent } from '../admin/agendamientos/progActividadServicio/prog-actividad-servicio.component';
import { CronogramaComponent } from '../admin/agendamientos/cronograma-agendar/cronograma.component';
import { UsuariosComponent } from '../admin/usuarios/usuarios.component';
import { AuthGuard } from '../admin/adminComponent/servicios/authGuard ';
import { EditarDatosComponent } from '../admin/usuarios/editar-datos/editar-datos.component';
import { MigracionComponent } from '../admin/migracion/migracion/migracion.component';


export const appRoutes: Routes = [
  /** servicios ********************************************************** */
  {
    path: 'admin', component: TemplateAdminComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'servicios', component: ServiciosComponent },
      { path: 'generarSuscripcion', component: GenerarSuscripcionComponent },
      { path: 'generarServicio', component: GenerarServicioComponent },
      { path: 'generarProgramacionDiaria', component: GenerarProgramamcionDiariaComponent },
      { path: 'AutorizarServicio', component: AutorizarServicioComponent },
      { path: 'GenerarServicioExtra', component: GenerarServicioExtraComponent },
      { path: 'GenerarServicioActividad', component: GenerarServicioActividadComponent },
      { path: 'TipoServicio', component: TiposServiciosComponent },
      /** cartera ********************************************************** */
      { path: 'clientesMora', component: ClientesMoraComponent },
      { path: 'consultarFactura', component: ConsultarfacturaComponent },
      { path: 'generaFactura', component: GenerarFacturaComponent },
      { path: 'generarPago', component: GenerarPagoComponent },
      { path: 'progActividadCorte', component: ProgramarActividadCorteComponent },
      { path: 'reportarCorte', component: ReportarCorteComponent },
      { path: 'inicioCartera', component: CarteraComponent },
      /**agendamioento ******************************************************** */
      { path: 'inicioAgendamiento', component: AgendamientosComponent },
      { path: 'cronogramaAgendamiento', component: CronogramaComponent },
      { path: 'registrarMaterial', component: RegistrarMaterialComponent },
      { path: 'serviciosAgnedados', component: ServiciosAgendadosComponent },
      { path: 'historialAgendamiento', component: HistorialAgendamientosComponent },
      { path: 'programarActividad', component: ProgActividadServicioComponent },

      /**EditarDatosComponent */
      { path: 'inicio', component: InicioAdminComponent },
      { path: 'usuarios', component: UsuariosComponent },
      { path: 'editarDatos', component: EditarDatosComponent },
      { path: 'migracion', component: MigracionComponent }


    ]
  },
  /** index **************************************************************** */
  { path: '', component: IndexComponent },
  { path: 'index', component: IndexComponent },
  { path: '**', component: ErrorNotFoundComponent },

];

