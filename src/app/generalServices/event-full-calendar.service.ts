import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventFullCalendarService {

  private date: Date;
  private yearMonth: string;
  private observable: Observable<any>;



  constructor() {
    this.date = new Date();
    this.yearMonth = this.date.getUTCFullYear() + '-' + (this.date.getUTCMonth() + 1);
  }

  public getEvents() {

    const data: any = [{
      DoctorsCount: 5,
      TotalAppointments: 20,
      Booked: 10,
      Cancelled: 2
    },
    {
      id: 98,
      title: 'Long Event111',
      start: this.yearMonth + '-09',
      end: this.yearMonth + '-10'
    },
    {
      id: 99,
      title: 'prueba',
      start: this.yearMonth + '-09'
    }];
    console.log(data);
    return this.observable = of(data);
  }
}
