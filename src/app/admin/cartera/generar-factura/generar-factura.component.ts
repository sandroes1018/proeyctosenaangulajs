import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-generar-factura',
  templateUrl: './generar-factura.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class GenerarFacturaComponent implements OnInit {


  public opcionesDataTable: any = {};

  constructor() { }

  ngOnInit() {
    this.cargarDataTables();
 }

   /**
     * cargarDataTables
     */
    public cargarDataTables() {
      this.opcionesDataTable = {
        dom: 'Bfrtip',
        buttons: [
          'print',
          'excel'
        ]
      };
  }

}
