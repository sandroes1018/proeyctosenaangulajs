import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FacturaService } from '../../adminComponent/servicios/factura-service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-consultarfactura',
  templateUrl: './consultarfactura.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class ConsultarfacturaComponent implements OnInit {

  public formBusquedaFactura: FormGroup;
  public datosFacturas: any;

  constructor(
    private formBuilder: FormBuilder,
    private facturaService: FacturaService
  ) { }

  ngOnInit() {
    this.construirFormBusquedaFactura();
  }

  private construirFormBusquedaFactura() {
    this.formBusquedaFactura = this.formBuilder.group({
      numeroDocumento: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(30)]],
    });
  }

  /**
   * consultarFacturasCliente
   */
  public consultarFacturasCliente() {
    let documento = this.formBusquedaFactura.getRawValue();
    documento = documento.numeroDocumento;
    this.facturaService.consultaFacturasCliente(documento).subscribe(
      result => {
        if (result.code !== 200) {
          this.datosFacturas = result;
          console.log( this.datosFacturas);
        }
      }, error => {
        console.error(<any>error);
        swal('Error!', 'No fue Posible Encontrar el numero de Documento', 'error');
      });
  }
}
