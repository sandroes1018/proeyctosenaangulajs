import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FacturaService } from '../../adminComponent/servicios/factura-service';
import swal from 'sweetalert2';
import { PagoService } from '../../adminComponent/servicios/pago-service';

@Component({
  selector: 'app-generar-pago',
  templateUrl: './generar-pago.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class GenerarPagoComponent implements OnInit {

  public saldoPago: number;
  public fechaPago: Date;
  public formRegistroFactura: FormGroup;
  public documento: FormGroup;
  public datosFacturaCliente: any;
  public datosCliente: any;

  constructor(
    private formBuilder: FormBuilder,
    private facturaService: FacturaService,
    private pagoService: PagoService
  ) { }

  ngOnInit() {
    this.saldoPago = 0;
    this.datosCliente = '';
    this.datosFacturaCliente = '';
    this.fechaPago = new Date;
    this.construirFormRegistroFactura();
    this.construirFormDocumento();
  }

  private construirFormDocumento() {
    this.documento = this.formBuilder.group({
      valor: [''],
    });
  }

  private construirFormRegistroFactura() {

    this.formRegistroFactura = this.formBuilder.group({
      numeroDocumento: [''], idFactura: [], saldoPago: [],
      fechaPago: [this.fechaPago],
      descripcion: ['', [Validators.required, Validators.maxLength(30)]],
      valor: ['', [Validators.required, Validators.pattern('[0-9]{2,9}')]],
    });
  }

  /**
   * datosCliente
   */
  public datosFacturaClienteRest() {
    const valor = this.documento.getRawValue();
    this.facturaService.consultarfacturaCliente(valor.valor)
      .subscribe(result => {
        if (result.code !== 200) {
          this.datosFacturaCliente = result;
          this.datosCliente = result.cuentaDto.idSuscripcion.idPersonaDto;
        }
      }, error => {
        console.error(<any>error);
        swal('Error!', 'Ha ocurrido un errro con la consulta', 'error');
      });
  }

  /**
   * calcularSaldo
   */
  public calcularSaldo(valor) {
    console.log(valor);
    this.saldoPago = this.datosFacturaCliente.saldo - valor;
  }

  /**
   * generarRegistroPago
   */
  public generarRegistroPago() {
    if (this.formRegistroFactura.valid) {
      if (this.saldoPago >= 0) {
        swal({
          title: 'Estar seguro?',
          text: 'Desea Realisar El Registro!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Generar!'
        }).then(result => {
          if (result.value) {
            this.registrarPago();
          }
        }
        );
      } else {
        swal('Error!', 'No se puede generar un pago con saldo negativo', 'error');
      }
    }
  }

  /**
   * registrarPagoRest
   */
  private registrarPago() {
    const id = this.datosFacturaCliente.idFactura;
    this.formRegistroFactura.controls['numeroDocumento'].setValue(this.datosCliente.numeroDocumento);
    this.formRegistroFactura.controls['idFactura'].setValue(id);
    this.formRegistroFactura.controls['saldoPago'].setValue(this.saldoPago);
    this.pagoService.registroPago(this.formRegistroFactura.getRawValue()).subscribe(
      resutl => {
        this.formRegistroFactura.reset();
        swal('Exito!', 'Generado Pago Correctamente', 'success');
      }, error => {
        swal('Error!', 'Ha ocurrido un errro con la consulta', 'error');
      }
    );
  }
}
