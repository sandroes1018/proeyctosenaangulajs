import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonaService } from '../../adminComponent/servicios/persona-service';

@Component({
  selector: 'app-clientes-mora',
  templateUrl: './clientes-mora.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class ClientesMoraComponent implements OnInit {

  public formCientesMora: FormGroup;
  public opcionesDataTable: any = {};
  public listaCientesMora: any;
  public datosCliente: any;
  public detalleFacturas: any;

  constructor(
    private formBuilder: FormBuilder,
    private personaService: PersonaService

  ) { }

  ngOnInit() {
    this.cargarDataTables();
    this.construirFormCientesMora();
    this.ListClientesMora();
  }

  /**
   * datosNotificacionCliente
   */
  public datosNotificacionCliente(datos: any) {
    this.datosCliente = datos;
  }

  /**
   * datosDetalleFacturas
   */
  public datosDetalleFacturas(datos: any) {
    this.detalleFacturas = datos.cuentaDto.facturaList;
    console.log(this.detalleFacturas);

  }

  public construirFormCientesMora() {
    this.formCientesMora = this.formBuilder.group({
      mensaje: ['', [Validators.required, Validators.maxLength(30)]],
    });
  }



  private ListClientesMora() {
    this.personaService.ListClientesMora().subscribe(
      result => {
        if (result.code !== 200) {
          console.log(result);
          this.listaCientesMora = result;
        }
      }, error => {
        console.log(<any>error);
      });
  }

  /**
    * cargarDataTables
    */
  public cargarDataTables() {
    this.opcionesDataTable = {
      dom: 'Bfrtip',
      buttons: [
        'print',
        'excel'
      ]
    };
  }
}
