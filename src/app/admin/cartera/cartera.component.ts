import { Component, OnInit } from '@angular/core';
import { MenuService } from '../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-cartera',
  templateUrl: './cartera.component.html',
  styleUrls: ['../adminComponent/template-admin/template-admin.component.css']
})
export class CarteraComponent implements OnInit {


  constructor(private menuService: MenuService) {
    sessionStorage.setItem('ubicacion', 'cartera');
    this.menuService.setMenu();
  }

  ngOnInit() {
  }

}
