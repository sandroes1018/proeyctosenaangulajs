import { Component, OnInit } from '@angular/core';
import { MenuService } from '../adminComponent/menu-header/serviceMenu';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-inicio-admin',
  templateUrl: './inicio-admin.component.html',
  styleUrls: ['../adminComponent/template-admin/template-admin.component.css']
})
export class InicioAdminComponent implements OnInit {


  constructor(
    public menuService: MenuService) {
    sessionStorage.setItem('ubicacion', 'inicio');
    this.menuService.setMenu();
  }

  ngOnInit() {

  }
}
