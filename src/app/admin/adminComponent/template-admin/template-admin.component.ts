import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu-header/serviceMenu';

@Component({
  selector: 'app-template-admin',
  templateUrl: './template-admin.component.html',
  styleUrls: ['./template-admin.component.css']
})
export class TemplateAdminComponent implements OnInit {

  public openMenuBody: string;
  public openContenido: string;
  public openMenu: string;
  public openHeader: string;

  constructor(public menuService: MenuService) {
  }
  ngOnInit() {
    this.openMenuBody = 'sidebar-is-reduced sidebar-is-expanded';
    this.openContenido = 'contenido_modulo_expand';
    this.openMenu = 'l-sidebar__content_expanded';
    this.openHeader = 'is-opened';
    this.menuService.setOpenContenido('contenido_modulo_expand');
  }

  public openMenuAdmin() {
    if (this.openMenu === '') {
      this.openMenu = 'l-sidebar__content_expanded';
      this.openHeader = 'is-opened';
      this.openMenuBody = 'sidebar-is-reduced sidebar-is-expanded';
      this.menuService.setOpenContenido('contenido_modulo_expand');
    } else {
      this.openMenu = '';
      this.openHeader = '';
      this.openMenuBody = '';
      this.menuService.setOpenContenido('');
    }
  }

}
