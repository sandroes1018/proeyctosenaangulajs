import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Constantes } from '../contantes';

@Injectable()
export class PagoService {


  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private http: HttpClient) {
  }

  public registroPago(fomrPago): Observable<any> {
    return this.http.put(Constantes.PAGO, fomrPago, this.httpOptions);
  }

}
