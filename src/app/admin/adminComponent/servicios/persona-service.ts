import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Constantes } from '../contantes';

@Injectable()
export class PersonaService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  constructor(private http: HttpClient) {
  }

  /**
   * crearTipoServicio
   */
  public crearClienteSuscripcion(serializedForm: any): Observable<any> {
    const data = JSON.stringify(serializedForm);
    return this.http.put(Constantes.persona + '/suscripcion', data, this.httpOptions);
  }

   /**
   * subirMigracion
   */
  public subirMigracion(listaArchivo: Array<any>): Observable<any> {
    const data = JSON.stringify(listaArchivo);
    return this.http.put(Constantes.persona + '/migracion', data, this.httpOptions);
  }

  /**
  * listarTipoServicios
  */
  public listarClientes(): Observable<any> {
    return this.http.get(Constantes.persona + '/clientes');
  }

  /**
   * elimiarTipoServicio
   */
  public elimiarPersona(id: string): Observable<any> {
    return this.http.delete(Constantes.persona + '/id/' + id, this.httpOptions);
  }
  /**
   * BusquedaCliente
   */
  public BusquedaCliente(datos): Observable<any> {
    console.log(datos.numeroDocumento);
    return this.http.get(Constantes.persona + '/cliente/' + datos.numeroDocumento);
  }

  public ListClientesMora(): Observable<any> {
    return this.http.get(Constantes.persona + '/clientes/mora');
  }

  /**
   * listaEmpleados
   */
  public listaEmpleados(): Observable<any> {
    return this.http.get(Constantes.persona + '/empleado', this.httpOptions);
  }

  /**
   * actualizarPersona
   */
  public actualizarPersonaUSuario(): Observable<any> {
    return this.http.put(Constantes.persona + '/usuario', this.httpOptions);
  }

  /**
   * actualizarPersona
   */
  public consultarPersonaUSuario(nombre: string): Observable<any> {
    return this.http.get(Constantes.persona + '/usuario/' + nombre, this.httpOptions);
  }

}
