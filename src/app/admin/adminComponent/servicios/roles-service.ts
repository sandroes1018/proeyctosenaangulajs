import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constantes } from '../contantes';

@Injectable()
export class RolesService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(
    private http: HttpClient,
  ) { }

  public crearROl(formfactura): Observable<any> {
    return this.http.post<JSON>(Constantes.ROL, formfactura);
  }

  public listaRoles(): Observable<any> {
    return this.http.get(Constantes.ROL);
  }

  /**
   * consultar factura Cliente
   */
  public consultarfacturaCliente(valor): Observable<any> {
    return this.http.get(Constantes.FACTURA + '/und/' + valor);
  }

}
