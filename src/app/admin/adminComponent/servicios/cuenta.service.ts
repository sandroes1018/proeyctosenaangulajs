import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Constantes } from '../contantes';

@Injectable({
  providedIn: 'root'
})
export class CuentaService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  constructor(private http: HttpClient) {
  }

    /**
   * subirMigracion
   */
  public subirMigracion(listaArchivo: Array<any>): Observable<any> {
    const data = JSON.stringify(listaArchivo);
    return this.http.put(Constantes.controlCuenta + '/migracion', data, this.httpOptions);
  }
}
