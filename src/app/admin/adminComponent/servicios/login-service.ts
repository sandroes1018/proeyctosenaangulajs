import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  public user: any;
  public session: String;
  private baseUrl: string;
  public data: any = [];
  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(
    private http: HttpClient,
    private router: Router) {
    this.session = sessionStorage.getItem('session');
    this.baseUrl = 'http://localhost:8080/user/login';
  }

  public loginUser(fomrLogin): Observable<any> {
    return this.http.post(this.baseUrl, fomrLogin, this.httpOptions);
  }

  public loginToken(): Observable<any> {
    return this.http.get(this.baseUrl + '/token');
  }

  public logOut() {
    sessionStorage.setItem('session', '');
    sessionStorage.getItem('session');
    this.session = '';
    this.router.navigate(['/']);
  }

  public isLogged() {
    if (this.session === 'true') {
      return true;
    } else {
      return false;
    }

  }

}
