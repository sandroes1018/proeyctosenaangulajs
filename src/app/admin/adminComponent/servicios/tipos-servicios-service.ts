import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constantes } from '../contantes';

@Injectable()
export class TipoServicioService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private http: HttpClient) {
  }

  /**
   * crearTipoServicio
   */
  public crearTipoServicio(serializedForm: any): Observable<any> {
    const data = JSON.stringify(serializedForm);
    return this.http.put<any>(Constantes.TIPO_SERVICIO, data, this.httpOptions);
  }

  /**
  * listarTipoServicios
  */
  public listarTipoServicios(): Observable<any> {
    return this.http.get(Constantes.TIPO_SERVICIO);
  }

  /**
   * elimiarTipoServicio
   */
  public elimiarTipoServicio(id: string): Observable<any> {
    return this.http.delete(Constantes.TIPO_SERVICIO + '/tipo/' + id, this.httpOptions);
  }

  /**
 * listarTipoServicios tv
 */
  public listarTipoServiciosTv(): Observable<any> {
    return this.http.get(Constantes.TIPO_SERVICIO + 'Tv');
  }

  /**
   * listarTipoServicios
   */
  public listarTipoServiciosInt(): Observable<any> {
    return this.http.get(Constantes.TIPO_SERVICIO + 'Int');
  }

  private handleError(error: any) {
    const errMsg = error.message || 'Server error';
    console.error(errMsg); // log to console instead
    console.log(errMsg);
    return Observable.throw(errMsg);

  }
}
