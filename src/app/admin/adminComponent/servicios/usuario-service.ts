import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constantes } from '../contantes';

@Injectable()
export class UsuarioService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private http: HttpClient) {
  }

  /**
   * crearTipoServicio
   */
  public crearUsuarioAmd(serializedForm: any): Observable<any> {
    const data = JSON.stringify(serializedForm);
    return this.http.put<JSON>(Constantes.USUARIO + '/admAplicacion', data, this.httpOptions);
  }

  /**
   * crearTipoServicio
   */
  public crearUsuarioCli(serializedForm: any): Observable<any> {
    console.log(serializedForm);
    const data = JSON.stringify(serializedForm);
    return this.http.put<JSON>(Constantes.USUARIO + '/cliAplicacion', data, this.httpOptions);
  }

  /**
     * crearTipoServicio
     */
  public recuperarPassword(serializedForm: any): Observable<any> {
    const data = serializedForm.correo;
    console.log(data);

    return this.http.get(Constantes.USUARIO + '/solicitud/' + data, this.httpOptions);
  }

}
