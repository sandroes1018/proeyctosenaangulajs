import { Injectable, ErrorHandler } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constantes } from '../contantes';

@Injectable()
export class SuscripcionService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(private http: HttpClient) {
  }

  /**
   * crearTipoServicio
   */
  public crearSuscripcion(serializedForm: any): Observable<any> {
    const data = JSON.stringify(serializedForm);
    return this.http.put<JSON>(Constantes.suscripcion, data, this.httpOptions);
  }

    /**
   * SuscripcionMigracion
   */
  public suscripcionMigracion(listaArchivo: Array<any>): Observable<any> {
    const data = JSON.stringify(listaArchivo);    
    return this.http.put<JSON>(Constantes.suscripcion + '/migracion', data, this.httpOptions);
  }

  /**
  * elimiarTipoServicio
  */
  public elimiarSuscripcion(id: string): Observable<any> {
    return this.http.delete(Constantes.suscripcion + '/tipo/' + id, this.httpOptions);
  }

   /**
  * listarTipoServicios
  */
  public listaTipoDocumento(): Observable<any> {
    return this.http.get(Constantes.tipoDocumento);
  }

  /**
   * Lista Sucursales
   */
  public ListaSucursales(): Observable<any> {
    return this.http.get(Constantes.sucursal);
  }
}
