import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Constantes } from '../contantes';

@Injectable()
export class FacturaService {

  private httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

  constructor(
    private http: HttpClient,
  ) { }

  public crearFactura(formfactura): Observable<any> {
    return this.http.post(Constantes.FACTURA, formfactura);
  }

  public consultaFacturasCliente(documento): Observable<any> {
    return this.http.get(Constantes.FACTURA + '/' + documento);
  }

  /**
   * consultar factura Cliente
   */
  public consultarfacturaCliente(valor): Observable<any> {
    return this.http.get(Constantes.FACTURA + '/und/' + valor);
  }

}
