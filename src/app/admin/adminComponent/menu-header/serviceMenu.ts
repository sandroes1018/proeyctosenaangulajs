import { Injectable } from '@angular/core';
import { RutasMenu } from './estructuraEnlacesMenu';
import { EnlacesMenu } from './enlacesMenu';

/** clase rutas */

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public rutas: RutasMenu[];
  public ubicacionRutaMenu: string;
  public enlacesMenu: EnlacesMenu;
  public openContenido: string;

  constructor() {
    this.enlacesMenu = new EnlacesMenu;
    this.ubicacionRutaMenu = sessionStorage.getItem('ubicacion');
    this.setMenu();
  }

  /**
  * listEnlacesMenu
  */
  public listEnlacesMenu(rutasMenu: RutasMenu[]) {
    this.rutas = rutasMenu;
  }

  /**
  * enlaza los enlaces del menu segun en el modulo
  * en que se encuentre el usuario
  */
  public setMenu() {
    this.ubicacionRutaMenu = sessionStorage.getItem('ubicacion');
    console.log(this.ubicacionRutaMenu);

    switch (this.ubicacionRutaMenu) {
      case 'cartera':
        this.listEnlacesMenu(this.enlacesMenu.enlacesMenuCartera);
        break;
      case 'controlServicios':
        this.listEnlacesMenu(this.enlacesMenu.enlacesMenuControlServicios);
        break;
      case 'agendamiento':
        this.listEnlacesMenu(this.enlacesMenu.enlacesMenuAgendamientos);
        break;
      case 'inicio':
        this.listEnlacesMenu(this.enlacesMenu.enlacesMenuInicio);
        break;
    }

  }

  /**
   * openContenido
   */
  public setOpenContenido(cssClass: string) {
    this.openContenido = cssClass;
  }

}
