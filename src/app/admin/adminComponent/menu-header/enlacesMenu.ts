import { RutasMenu } from './estructuraEnlacesMenu';

export class EnlacesMenu {

  constructor() { }

  public enlacesMenuInicio = [
    new RutasMenu('CARTERA', 'inicioCartera', 'glyphicon glyphicon-briefcase'),
    new RutasMenu('SERIVICIOS', 'servicios', 'glyphicon glyphicon-list-alt'),
    new RutasMenu('AGENDAMIENTOS', 'inicioAgendamiento', 'glyphicon glyphicon-calendar'),
    new RutasMenu('USUARIOS', 'usuarios', 'glyphicon glyphicon-user'),
    new RutasMenu('DIC. DATOS', 'diccionarioDatos', 'glyphicon glyphicon-floppy-disk'),
    new RutasMenu('USUARIOS', 'usuarios', 'glyphicon glyphicon-user'),
    new RutasMenu('Migracion', 'migracion', 'glyphicon glyphicon-user')
  ];

  public enlacesMenuControlServicios = [
    new RutasMenu('INICIO', 'servicios', 'glyphicon glyphicon-home'),
    new RutasMenu('GENERAR ACTIVIDAD', 'generarServicio', 'glyphicon glyphicon-edit'),
    new RutasMenu('GENERAR SUSCRIPCION', 'generarSuscripcion', 'glyphicon glyphicon-list-alt'),
    new RutasMenu('GENERAR PROG DIARIA', 'generarProgramacionDiaria', 'glyphicon glyphicon-calendar'),
    new RutasMenu('APROBAR ACTIVIDAD', 'AutorizarServicio', 'glyphicon glyphicon-briefcase'),
    new RutasMenu('SERVICIOS', 'TipoServicio', 'glyphicon glyphicon-briefcase')
  ];

  public enlacesMenuAgendamientos = [
    new RutasMenu('INICIO', 'inicioAgendamiento', 'glyphicon glyphicon-home'),
    new RutasMenu('HISTORIAL AGENDAMIENTOS', 'historialAgendamiento', 'glyphicon glyphicon-time'),
    new RutasMenu('REGISTRAR MATERIAL', 'registrarMaterial', 'glyphicon glyphicon-floppy-saved'),
    new RutasMenu('SERVICIOS AGENDADOS', 'serviciosAgnedados', 'glyphicon glyphicon-calendar'),
    new RutasMenu('CRONOGRAMA', 'cronogramaAgendamiento', 'glyphicon glyphicon-calendar'),
  ];

  public enlacesMenuCartera = [
    new RutasMenu('INICIO', 'inicioCartera', 'glyphicon glyphicon-home'),
    new RutasMenu('CONSULTAR FACTURA', 'consultarFactura', 'glyphicon glyphicon-zoom-in'),
    new RutasMenu('CLIENTES EN MORA', 'clientesMora', 'glyphicon glyphicon-list-alt'),
    new RutasMenu('GENERAR fACTURA', 'generaFactura', 'glyphicon glyphicon-edit'),
   /*  new RutasMenu('GENERAR PROG-CORTE', 'progActividadCorte', 'glyphicon glyphicon-briefcase'),
    new RutasMenu('GENERAR REPORTAR-CORTE', 'reportarCorte', 'glyphicon glyphicon-edit'), */
    new RutasMenu('GENERAR INGRESAR PAGO', 'generarPago', 'glyphicon glyphicon-briefcase')
  ];


}
