import { Component, OnInit } from '@angular/core';
import { MenuService } from './serviceMenu';
import { LoginService } from '../servicios/login-service';

@Component({
  selector: 'app-menu-header',
  templateUrl: './menu-header.component.html',
  styleUrls: ['./../template-admin/template-admin.component.css']
})

export class MenuHeaderComponent implements OnInit {

  public openMenuBody: string;
  public openContenido: string;
  public openMenu: string;
  public openHeader: string;
  public usuario: string;

  constructor(
    public menuService: MenuService,
    public loginService: LoginService
  ) {
    this.openMenuBody = 'sidebar-is-reduced sidebar-is-expanded';
    this.openContenido = 'contenido_modulo_expand';
    this.openMenu = 'l-sidebar__content_expanded';
    this.openHeader = 'is-opened';
    this.usuario = sessionStorage.getItem('usuario');
    this.menuService.setOpenContenido('contenido_modulo_expand');

  }

  ngOnInit() {
  }

  /**
   * openMenuAdmin
   * amplia el menu admin
   * jmiguel
   */
  public openMenuAdmin() {
    if (this.openMenu === '') {
      this.openMenu = 'l-sidebar__content_expanded';
      this.openHeader = 'is-opened';
      this.openMenuBody = 'sidebar-is-reduced sidebar-is-expanded';
      this.menuService.setOpenContenido('contenido_modulo_expand');
    } else {
      this.openMenu = '';
      this.openHeader = '';
      this.openMenuBody = '';
      this.menuService.setOpenContenido('');
    }
  }
}
