export class Constantes {

  // tslint:disable-next-line:quotemark
  private static BASE_URL = 'http://localhost:8080';
  constructor() {
  }
  public static controlCuenta = Constantes.BASE_URL + '/cu/cuenta';
  public static controlServicios = Constantes.BASE_URL + '/cs/tipoServicio';
  public static tipoDocumento =  Constantes.BASE_URL + '/td/tipoDocumento';
  public static suscripcion =  Constantes.BASE_URL + '/sus/suscripcion';
  public static sucursal =  Constantes.BASE_URL + '/suc/sucursal';
  public static persona =  Constantes.BASE_URL + '/per/persona';
  public static TIPO_SERVICIO = Constantes.BASE_URL + '/cs/controlServicio';
  public static FACTURA = Constantes.BASE_URL + '/fac/factura';
  public static PAGO = Constantes.BASE_URL + '/pag/pago';
  public static USUARIO = Constantes.BASE_URL + '/us/usuario';
  public static ROL = Constantes.BASE_URL + '/cargo/tipo';






}
