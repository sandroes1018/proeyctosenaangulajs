import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { PersonaService } from '../../adminComponent/servicios/persona-service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import swal from 'sweetalert2';
import { SuscripcionService } from '../../adminComponent/servicios/suscripcion-service';

@Component({
  selector: 'app-editar-datos',
  templateUrl: './editar-datos.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class EditarDatosComponent implements OnInit {


  public listaTipoDoumento: any;
  public listaSucursales: any;
  public formEditDatos: FormGroup;
  private datoPersona: any;

  constructor(
    public menuService: MenuService,
    private personaService: PersonaService,
    private formBuilder: FormBuilder,
    private suscripcionService: SuscripcionService
  ) {
    this.consultaDatosPersona();
  }

  ngOnInit() {
    this.listaTipoDocumentoRest();
    this.ListaSucursalesRest();
    this.construirFormEditDatos();
  }

  private construirFormEditDatos() {
    this.formEditDatos = this.formBuilder.group({
      idTipo: ['', [Validators.required, Validators.maxLength(30)]],
      numeroDocumento: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      apellido: ['', [Validators.required, Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.maxLength(30), Validators.email]],
      direccion: ['', [Validators.required, Validators.maxLength(30)]],
      telefono: ['', [Validators.required, Validators.maxLength(30)]],
      cargo: [''],
      idSucursal: ['', [Validators.required, Validators.maxLength(30)]],
    });
   // this.setValoresFormEditDatos();
  }

  /**
 * lista Tipo Documento
 */
  public listaTipoDocumentoRest() {
    this.suscripcionService.listaTipoDocumento()
      .subscribe(
        result => {
          if (result.code !== 200) {
            this.listaTipoDoumento = result;
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al cargar datos', 'error');
        });
  }

  /**
   * Lista Sucursales
   */
  private ListaSucursalesRest() {
      this.suscripcionService.ListaSucursales()
        .subscribe(
          result => {
            if (result.code !== 200) {
              this.listaSucursales = result;
              console.log(result);

            }
          },
          error => {
            console.error(<any>error.status);
            swal('Error!', error.error.message, 'error');
          });
  }

  /**
   * Lista Sucursales
   */
  public onSubmitEditarDatos() {
    this.personaService.actualizarPersonaUSuario()
      .subscribe(
        result => {
          if (result.code !== 200) {
            swal('Exito!', result.messager, 'success');
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al cargar datos', 'error');
        });
  }

  /**
 * lista Tipo Documento
 */
  public consultaDatosPersona() {
    this.personaService.consultarPersonaUSuario(sessionStorage.getItem('usuario'))
      .subscribe(result => {
        if (result.code !== 200) {
          this.datoPersona = result;
          console.log(this.datoPersona);
        }
      }, error => {
        console.error(<any>error.status);
        swal('Error!', 'error al cargar datos', 'error');
      });
  }
}
