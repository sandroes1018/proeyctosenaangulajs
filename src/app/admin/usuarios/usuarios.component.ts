import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from '../adminComponent/servicios/usuario-service';
import swal from 'sweetalert2';
import { PersonaService } from '../adminComponent/servicios/persona-service';
import { RolesService } from '../adminComponent/servicios/roles-service';
import { ButtonClickModel } from 'ng-fullcalendar';
import { empty, from } from 'rxjs';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['../adminComponent/template-admin/template-admin.component.css']
})

export class UsuariosComponent implements OnInit {

  public listaTipoDoumento: any;
  public formUsuario: FormGroup;
  public listaEmpleados: any;
  public datosEmpleado: any;
  public rolesLista: any;
  public data: any;

  

  constructor(
    private formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private paersonaService: PersonaService,
    private rolesService: RolesService
  ) {
  }


  ngOnInit() {
    /*  this.menuService.setMenu('controlServicios'); */
    this.construirFormUsuario();
    this.listaEmpleadosGet();
    this.listaRolesGet();
  }

  /**
    * construir formualario con campos y validaciones
    */
  private construirFormUsuario() {
    this.formUsuario = this.formBuilder.group({
      idPersona: [],
      rol: ['', [Validators.required, Validators.maxLength(30)]],
      empleado: ['', [Validators.required]],
      nombre: ['', [Validators.required, Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
    });
  }


  /**
   * submit formualio crear tipo servicio
   * onSubmitformTipoServicio
   */
  public onSubmitformUsuario() {
    if (this.formUsuario.valid) {
      this.formUsuario.controls['idPersona'].setValue(this.datosEmpleado.idPersona);
      console.log(this.formUsuario.getRawValue());
      this.usuarioService.crearUsuarioAmd(this.formUsuario.getRawValue())
        .subscribe(
          result => {
            if (result.code !== 200) {
              this.formUsuario.reset();
              swal('Exito!', result.message, 'success');
            }
          }, error => {
            console.log(<any>error);
            swal('Error!', error.error.message, 'error');
          }
        );
    }
  }

  /**
  * slistaEmpleadosGet
  */
  public listaEmpleadosGet() {
    this.paersonaService.listaEmpleados().subscribe(
      result => {
        if (result.code !== 200) {
          this.listaEmpleados = result;
        }
      }, error => {
        console.log(<any>error);
        swal('Error!', 'Error al cargar datos empleado', 'error');
      }
    );
  }

  /**
   * listaRoles
   */
  public listaRolesGet() {
    this.rolesService.listaRoles().subscribe(
      result => {
        if (result.code !== 200) {
          this.rolesLista = result;
        }
      }, error => {
        console.log(<any>error);
        swal('Error!', 'Error al cargar datos rol', 'error');
      }
    );
  }

  /**
   * seleccionEmpleado
   */
  public seleccionEmpleado(empleado: any) {
    this.listaEmpleados.forEach(emp => {
      if (emp.numeroDocumento === empleado) {
        this.datosEmpleado = emp;
      }
    });
  }
}
