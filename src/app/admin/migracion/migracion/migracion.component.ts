import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { PersonaService } from '../../adminComponent/servicios/persona-service';
import { SuscripcionService } from '../../adminComponent/servicios/suscripcion-service';
import { CuentaService } from '../../adminComponent/servicios/cuenta.service';


@Component({
  selector: 'app-migracion',
  templateUrl: './migracion.component.html',
  styleUrls: ['./migracion.component.css']
})
export class MigracionComponent implements OnInit {
  public lines=[];
  public nombreFile:string="SELECCIONE ARCHIVO DE MIGRACION";

  
  constructor(private paersonaService: PersonaService,private suscripcionService: SuscripcionService,private cuentaService: CuentaService) { }

  ngOnInit() {
  }

  public changeListener(files: FileList){

    console.log(files);
    if(files && files.length > 0) {
       let file : File = files.item(0);        
          this.nombreFile=file.name;        
         console.log(file.name);
         console.log(file.size);
         console.log(file.type);
         this.leerArchivo(file);                
      }
  }

  private leerArchivo(file):void{
    let reader: FileReader = new FileReader();
         reader.readAsText(file);        
  
         reader.onload = (e) => {
          let csv: any = reader.result;  
          let allTextLines = csv.split(/\r|\n|\r/);  
          let headers = allTextLines[0].split(';');  
          this.lines = [];  
            
          for (let i = 0; i < allTextLines.length; i++) {  
          // split content based on comma  
          let data = allTextLines[i].split(';');  
          if (data.length === headers.length) {  
          let tarr = [];  
          for (let j = 0; j < headers.length; j++) {  
          tarr.push(data[j]);  
          }            
          // log each row to see output  
          console.log(tarr);  
          this.lines.push(tarr);  
          }  
          }  
          // all rows in the csv file
          console.log(">>>>>>>>>>>>>>>>>", this.lines);      
         }
  }
  
    public Subir() {    
      if(this.lines.length<=0){
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'Favor Seleccione un archivo para la migracion'       
        })
      }else{   
        const swalWithBootstrapButtons = swal.mixin({
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false,
        })   
      swalWithBootstrapButtons({
        title: 'Migracion',
        text: "Desea realizar la migracion de "+this.lines.length+' registros?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Iniciar migracion',
        cancelButtonText: 'No, cancelar',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {

          switch (this.nombreFile) {
              case "cliMigracion.csv":
              this.iniciarMigracionClientes();
              break;
              case "susMigracion.csv":
              this.iniciarMigracionSuscripcion();
              break;
              case "cuMigracion.csv":
              this.iniciarMigracionCuenta();
              break;
          
            default:
              break;
          }
          
         
        } else if (
          // Read more about handling dismissals
          result.dismiss === swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons(
            'Cancelada',
            'Migracion cancelada',
            'error'
          )
        }
      }) 
    } 
    }

    private iniciarMigracionCuenta(){
      let lista:Array<any>=[];
      
      for (let i = 0; i < this.lines.length; i++) {        
        let obj={
              "estadoCuen":this.lines[i][0],
              "idenCliente":this.lines[i][1],                        
            };     
          lista.push(obj);          
      }
      console.log(lista);
      this.cuentaService.subirMigracion(lista).subscribe(
        result => {
          if (result.code !== 200) {
           console.log(result);
           swal(
            'Exito!',
            'Migracion Exitosa',
            'success'
          )
          }
        }, error => {
          console.log(<any>error);
          swal('Error!', 'Error al cargar la migracion', 'error');
        }
      );
    }

    private iniciarMigracionSuscripcion(){
        let lista:Array<any>=[];
      
      for (let i = 0; i < this.lines.length; i++) {        
        let obj={
              "valorTotal":this.lines[i][0],
              "fecha":this.lines[i][1],
              "idPersonaDto":{
                "numeroDocumento":this.lines[i][2]
              }            
            };     
          lista.push(obj);          
      }
      console.log(lista);

     this.suscripcionService.suscripcionMigracion(lista).subscribe(
        result => {
          if (result.code !== 200) {
           console.log(result);
           swal(
            'Exito!',
            'Migracion Exitosa',
            'success'
          )
          }
        }, error => {
          console.log(<any>error);
          swal('Error!', 'Error al cargar la migracion', 'error');
        }
      );
    }
  
    private iniciarMigracionClientes(){
    
      let lista:Array<any>=[];
      
      for (let i = 0; i < this.lines.length; i++) {        
        let obj={
              "numeroDocumento":this.lines[i][0],
              "nombre":this.lines[i][1],
              "apellido":this.lines[i][2],
              "direccion":this.lines[i][3],
              "telefono":this.lines[i][4],
              "email":this.lines[i][6],
              "idTipo":this.lines[i][7]
            };     
          lista.push(obj);
      }
      this.paersonaService.subirMigracion(lista).subscribe(
        result => {
          if (result.code !== 200) {
           console.log(result);
           swal(
            'Exito!',
            'Migracion Exitosa',
            'success'
          )
          }
        }, error => {
          console.log(<any>error);
          swal('Error!', 'Error al cargar la migracion', 'error');
        }
      );
      
    }

}
