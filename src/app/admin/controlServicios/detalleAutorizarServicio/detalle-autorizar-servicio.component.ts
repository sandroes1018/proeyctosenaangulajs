import { Component, OnInit } from '@angular/core';

import { MenuService } from '../../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-detalle-autorizar-servicio',
  templateUrl: './detalle-autorizar-servicio.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class DetalleAutorizarServicioComponent implements OnInit {

  constructor(public menuService: MenuService) { }

  ngOnInit() {
  }

}
