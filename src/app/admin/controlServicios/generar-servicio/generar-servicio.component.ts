import { Component, OnInit } from '@angular/core';

import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonaService } from '../../adminComponent/servicios/persona-service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-generar-servicio',
  templateUrl: './generar-servicio.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class GenerarServicioComponent implements OnInit {

  public opcionesDataTable: any = {};
  public listaClientes: any;

  constructor(
    private personaService: PersonaService
    ) { }

  ngOnInit() {
    this.listaClientesRest();
  }

   /**
   * lista Clientes
   */
  private listaClientesRest() {
    this.personaService.listarClientes().subscribe(
      result => {
        if (result.code !== 200) {
          this.listaClientes = result;
          console.log(this.listaClientes);
        }
      }, error => {
        console.error(<any>error);
        swal('Error!', 'Error al cargar los datos!', 'error');
      }
    );
  }
}
