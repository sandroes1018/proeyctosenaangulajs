import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { SuscripcionService } from '../../adminComponent/servicios/suscripcion-service';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TipoServicioService } from '../../adminComponent/servicios/tipos-servicios-service';
import { PersonaService } from '../../adminComponent/servicios/persona-service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-generar-suscripcion',
  templateUrl: './generar-suscripcion.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})

export class GenerarSuscripcionComponent implements OnInit {

  public listaTipoDoumento: any;
  public listaSucursales: any;
  public formSuscripcion: FormGroup;
  public listaServicios: any;
  public valorServicios: number;
  private servicioSuscripcion = [];

  constructor(
    public menuService: MenuService,
    private suscripcionService: SuscripcionService,
    private tipoServicioService: TipoServicioService,
    private personaService: PersonaService,
    private formBuilder: FormBuilder,
  ) {
    this.valorServicios = 0;
  }


  ngOnInit() {
    /*  this.menuService.setMenu('controlServicios'); */

    this.construirFormSuscripcion();
    this.listaTipoDocumentoRest();
    this.ListaSucursalesRest();
    this.ListaServiciosRest();
  }

  /**
   * agregar sevicios
   */
  public agregarSevicios(e, idTp: number, val: number) {
    if (e.target.checked) {
      this.servicioSuscripcion.push(idTp);
      this.valorServicios += val;

    } else {
      const index = this.servicioSuscripcion.indexOf(idTp);
      if (index > -1) {
        this.servicioSuscripcion.splice(index, 1);
      }
      this.valorServicios -= val;
    }

  }

  /**
    * construir formualario con campos y validaciones
    */

  private construirFormSuscripcion() {
    this.formSuscripcion = this.formBuilder.group({
      valorTotal: [],
      idTipo: ['', [Validators.required, Validators.maxLength(30)]],
      numeroDocumento: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      apellido: ['', [Validators.required, Validators.maxLength(30)]],
      email: ['', [Validators.required, Validators.maxLength(30), Validators.email]],
      direccion: ['', [Validators.required, Validators.maxLength(30)]],
      telefono: ['', [Validators.required, Validators.maxLength(30)]],
      cargo: ['cliente'],
      idSucursal: ['', [Validators.required, Validators.maxLength(30)]],
      serviSuscripcion: [this.servicioSuscripcion],
    });
  }

  /**
   * submit formualio crear tipo servicio
   * onSubmitformTipoServicio
   */
  public onSubmitformSuscripcion() {
    if (this.formSuscripcion.valid) {
      this.formSuscripcion.controls['valorTotal'].setValue(this.valorServicios);
      console.log(this.servicioSuscripcion);
      console.log(this.formSuscripcion.getRawValue());
      if (this.servicioSuscripcion.length > 0) {
        this.personaService.crearClienteSuscripcion(this.formSuscripcion.getRawValue())
          .subscribe(
            result => {
              if (result.code !== 200) {
                this.formSuscripcion.reset();
                this.servicioSuscripcion = null;
                swal('Exito!', 'Servicio creado correctamente', 'success');
              }
            }, error => {
              console.log(<any>error);
              swal('Error!', error.error.message, 'error');
            }
          );
      } else {
        swal('Error!', 'selecciones algun servicio', 'error');
      }
    }
  }

  /**
   * lista Tipo Documento
   */
  public listaTipoDocumentoRest() {
    this.suscripcionService.listaTipoDocumento()
      .subscribe(
        result => {
          if (result.code !== 200) {
            this.listaTipoDoumento = result;
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al cargar datos', 'error');
        });
  }

  /**
   * Lista Sucursales
   */
  private ListaSucursalesRest() {
    this.suscripcionService.ListaSucursales()
      .subscribe(
        result => {
          if (result.code !== 200) {
            this.listaSucursales = result;
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al cargar datos', 'error');
        });
  }

  /**
   * Lista Sucursales tv
   */
  private ListaServiciosRest() {
    this.tipoServicioService.listarTipoServicios()
      .subscribe(
        result => {
          if (result.code !== 200) {
            this.listaServicios = result;
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al cargar datos', 'error');
        });
  }


}

