import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-generar-programamcion-diaria',
  templateUrl: './generar-programamcion-diaria.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class GenerarProgramamcionDiariaComponent implements OnInit {


  public opcionesDataTable: any = {};

  constructor(public menuService: MenuService) { }

  ngOnInit() {
   /*  this.menuService.setMenu('controlServicios'); */
    sessionStorage.setItem('ubicacion', 'controlServicios');

    this.cargarDataTables();
  }

    /**
     * cargarDataTables
     */
    public cargarDataTables() {
      this.opcionesDataTable = {
        dom: 'Bfrtip',
        buttons: [
          'print',
          'excel'
        ]
      };
  }

}


