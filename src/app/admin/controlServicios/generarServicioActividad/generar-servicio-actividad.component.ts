import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';


@Component({
  selector: 'app-generar-servicio-actividad',
  templateUrl: './generar-servicio-actividad.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class GenerarServicioActividadComponent implements OnInit {

  public linkImgUser: string;

  constructor(public menuService: MenuService) {

  }

  ngOnInit() {
    /* this.menuService.setMenu('controlServicios'); */
    sessionStorage.setItem('ubicacion', 'controlServicios');

  }


}
