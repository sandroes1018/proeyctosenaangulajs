import { Component, OnInit } from '@angular/core';
import { MenuService } from '../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['../adminComponent/template-admin/template-admin.component.css']
})
export class ServiciosComponent implements OnInit {


  constructor(public menuService: MenuService) {
    sessionStorage.setItem('ubicacion', 'controlServicios');
    this.menuService.setMenu();
  }

  ngOnInit() {


  }

}
