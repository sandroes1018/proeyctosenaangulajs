import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { TipoServicioService } from '../../adminComponent/servicios/tipos-servicios-service';



declare var $: any; /*ejecutar moval bootsatrap*/

@Component({
  selector: 'app-tipos-sericios',
  templateUrl: './tipos-servicios.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class TiposServiciosComponent implements OnInit {

  public opcionesDataTable: any = {};
  public formTipoServicio: FormGroup;
  public listaTiposervicios: any;

  constructor(
    private formBuilder: FormBuilder,
    public tipoServicioService: TipoServicioService
  ) { }

  ngOnInit() {
    this.construirFormTipoServicio();
    this.listaTipoServiciosRest();
  }

  /**
   * construirFormTipoServicio
   */
  public construirFormTipoServicio() {
    this.formTipoServicio = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.maxLength(30)]],
      descripcion: ['', [Validators.required, Validators.maxLength(30)]],
      valor: ['', [Validators.required]],
    });
  }

  /**
   * submit formualio crear tipo servicio
   * onSubmitformTipoServicio
   */
  public onSubmitformTipoServicio() {
    if (this.formTipoServicio.valid) {
      this.tipoServicioService.crearTipoServicio(this.formTipoServicio.getRawValue())
        .subscribe(
          result => {
            if (result.code !== 200) {
              this.formTipoServicio.reset();
              this.listaTiposervicios = null;
              this.listaTipoServiciosRest();
              swal('Exito!', 'Servicio creado correctamente', 'success');
            }
          },
          error => {
            console.log(<any>error);
            swal('Error!', 'Ha ocurrido un error', 'error');
          }
        );
    }
  }

  /**
   * lista tiposservicios
   * listaTipoServicios
   */
  public listaTipoServiciosRest() {
    this.tipoServicioService.listarTipoServicios()
      .subscribe(
        result => {
          if (result.code !== 200) {
            this.listaTiposervicios = result;
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al cargar los datos', 'error');
        });
  }

  /**
   * eliniar tiposervicio de la base de datos
   * eliminarTipoServicio
   */
  public preEliminar(id: string) {

    swal({
      title: 'Estar seguro?',
      text: 'Desea Eliminar!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Borrar!'
    }).then(result => {
      if (result.value) {
        this.eliminarTipoServicio(id);
      }
    }
    );
  }

  /**
   * preEliminar
   */
  public eliminarTipoServicio(id: string) {
    this.tipoServicioService.elimiarTipoServicio(id)
      .subscribe(
        result => {
          if (result.code !== 200) {
            swal('Exito!', 'Servicio eliminado correctamente', 'success');
            this.listaTiposervicios = null;
            this.listaTipoServiciosRest();
          }
        },
        error => {
          console.error(<any>error.status);
          swal('Error!', 'error al eliminar', 'error');
        });
  }
}
