import { Component, OnInit } from '@angular/core';

import { MenuService } from '../../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-autorizar-servicio',
  templateUrl: './autorizar-servicio.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class AutorizarServicioComponent implements OnInit {

  public opcionesDataTable: any = {};


  constructor() { }

  ngOnInit() {
  }

  /**
   * cargarDataTables
   */
  public cargarDataTables() {
    this.opcionesDataTable = {
      dom: 'Bfrtip',
    };
  }

}
