import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';

/** services */
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { EventFullCalendarService } from '../../../generalServices/event-full-calendar.service';

@Component({
  selector: 'app-programar-actividad-servicio',
  templateUrl: './prog-actividad-servicio.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class ProgActividadServicioComponent implements OnInit {

  @ViewChild(CalendarComponent) ucCalendar: CalendarComponent;

  public calendarOptions: Options;
  public events: any[];

  constructor(
    public menuService: MenuService,
    public eventService: EventFullCalendarService
  ) { }

  ngOnInit() {
    /* this.menuService.setMenu('controlServicios'); */
    this.loadEvents();
    this.caragrFullCalendar();
  }

  /**
   * cargar todos los eventos y opcines fullcalendar
   * caragrFullCalendar
   */
  public caragrFullCalendar() {
    this.calendarOptions = {
      editable: true,
      eventLimit: false,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
      },
      selectable: true,
      events: this.events,
    };
  }

  /**
    * cargar todos los eventos del service fullcalendar
    * caragrFullCalendar
    */
  loadEvents() {
    this.eventService.getEvents().subscribe(data => {
      this.events = data;
    });
  }

  clearEvents() {
    this.events = [];
  }

  eventClick(item) {
    console.log(item);
  }

  clickButton(item) {
    console.log(item);
  }

  updateEvent(item) {
    console.log(item);
  }
}
