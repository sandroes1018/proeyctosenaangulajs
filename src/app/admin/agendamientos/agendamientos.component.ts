import { Component, OnInit } from '@angular/core';

import { MenuService } from '../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-agendamientos',
  templateUrl: './agendamientos.component.html',
  styleUrls: ['../adminComponent/template-admin/template-admin.component.css']
})
export class AgendamientosComponent implements OnInit {


  constructor(public menuService: MenuService) {
    sessionStorage.setItem('ubicacion', 'agendamiento');
    this.menuService.setMenu();
  }

  ngOnInit() {
  }

}
