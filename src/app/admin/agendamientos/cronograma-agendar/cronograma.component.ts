import { Component, OnInit } from '@angular/core';

import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { Options } from 'fullcalendar';

@Component({
  selector: 'app-cronograma-agendar',
  templateUrl: './cronograma.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class CronogramaComponent implements OnInit {

  public calendarOptions: Options;
  public events: any[];

  constructor(public menuService: MenuService) { }

  ngOnInit() {
    this.caragrFullCalendar();
  }


  /**
   * cargar todos los eventos y opcines fullcalendar
   * caragrFullCalendar
   */
  public caragrFullCalendar() {
    this.calendarOptions = {
      editable: true,
      eventLimit: false,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listMonth'
      },
      selectable: true,
      events: this.events,
    };
  }
}
