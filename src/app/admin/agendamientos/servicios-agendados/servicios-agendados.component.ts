import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';

@Component({
  selector: 'app-servicios-agendados',
  templateUrl: './servicios-agendados.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class ServiciosAgendadosComponent implements OnInit {

  public opcionesDataTable: any = {};

  constructor(public menuService: MenuService) { }

  ngOnInit() {
  }

}
