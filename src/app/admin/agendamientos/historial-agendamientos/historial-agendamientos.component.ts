import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../adminComponent/menu-header/serviceMenu';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-historial-agendamientos',
  templateUrl: './historial-agendamientos.component.html',
  styleUrls: ['../../adminComponent/template-admin/template-admin.component.css']
})
export class HistorialAgendamientosComponent implements OnInit {

  public opcionesDataTable: any = {};
  public formRangoFechas: any;

  constructor(
    public menuService: MenuService,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    this.construirFormRangoFechas();
  }

  public construirFormRangoFechas() {
    this.formRangoFechas = this.formBuilder.group({
      fechaInicio: ['', [Validators.required, Validators.maxLength(30)]],
      fechaFin: ['', [Validators.required, Validators.maxLength(30)]]
    });
  }

}
