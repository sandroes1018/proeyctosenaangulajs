import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  public formContactenos: FormGroup;
  constructor( private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.construirFormContactenos();
  }


  private construirFormContactenos() {
    this.formContactenos = this.formBuilder.group({
      nombre_contacto: ['', [Validators.required, Validators.minLength(5)]],
      correo_contacto: ['', [Validators.required, Validators.maxLength(5)]],
      telefono_contacto: ['', [Validators.required, Validators.minLength(5)]],
      mensaje_contacto: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  /**
   * onSubmitFormTipoServicio
   */
  public onSubmitFormContactenos() {
    console.log(this.formContactenos);
  }
}
