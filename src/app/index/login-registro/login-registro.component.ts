import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../admin/adminComponent/servicios/login-service';
import { UsuarioService } from '../../admin/adminComponent/servicios/usuario-service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-registro',
  templateUrl: './login-registro.component.html',
  styleUrls: ['../index.component.css']
})
export class LoginRegistroComponent implements OnInit {

  @ViewChild('completeModal') public completeModal: ElementRef;

  public registro = false;
  public recuperarPassword = false;
  public login = true;
  public formLogin: FormGroup;
  public formRegistro: FormGroup;
  public formRecuperarPassword: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private usuarioService: UsuarioService
  ) {

  }

  ngOnInit() {
    this.construirFormRecuperarPassword();
    this.construirFormRegistro();
    this.construirFormLogin();
  }
  /**
     * login
     */
  private construirFormLogin() {
    this.formLogin = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  /**
   * login
   */
  private construirFormRecuperarPassword() {
    this.formRecuperarPassword = this.formBuilder.group({
      correo: ['', [Validators.required, Validators.email]],
    });
  }

  /**
   * login
   */
  private construirFormRegistro() {
    this.formRegistro = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(5)]],
      numeroDocumento: ['', [Validators.required, Validators.minLength(5)]],
    });
  }

  /**
   * mostrarLogin
   */
  public mostrarLogin() {
    this.login = true;
    this.recuperarPassword = false;
    this.registro = false;
  }

  /**
   * mostrarRegistro
   */
  public mostrarRegistro() {
    this.login = false;
    this.recuperarPassword = false;
    this.registro = true;
  }

  public mostrarRecuperarPassword() {
    this.login = false;
    this.recuperarPassword = true;
    this.registro = false;
  }

  /**
   * onSubmitFormLogin
   */
  public onSubmitFormRegistro() {
    if (this.formRegistro.valid) {
      console.log(this.formRegistro.getRawValue());
      this.usuarioService.crearUsuarioCli(this.formRegistro.getRawValue())
        .subscribe(result => {
          if (result.code !== 200) {
            console.log(result);

            swal('Exito!', result.messenger, 'success');
          }
        }, error => {
          console.error(<any>error);
          swal('Error!', error.error.message, 'error');
        });
    }
  }

  public onSubmitFormLogin() {
    if (this.formLogin.valid) {
      console.log(this.formLogin.getRawValue());
      this.loginService.loginUser(this.formLogin.getRawValue())
        .subscribe(result => {
          if (result.code !== 200) {
            console.log(result);
            sessionStorage.setItem('usuario', result.usuario);
            if (result.ruta === 'AMD') {
              location.reload(true);
              this.router.navigate(['/admin/inicio']);
              this.loginService.session = 'true';
              sessionStorage.setItem('session', 'true');
            } else if (result.ruta === 'CLI') {

            }
          }
        }, error => {
          console.error(<any>error);
          swal('Error!', error.error.message, 'error');
        });
    }
  }

  /**
   * onSubmitFormLogin
   */
  public onSubmitFormRecuperarPassword() {
    if (this.formRecuperarPassword.valid) {
      console.log(this.formRecuperarPassword.getRawValue());
      this.usuarioService.recuperarPassword(this.formRecuperarPassword.getRawValue())
        .subscribe(result => {
          if (result.code !== 200) {
            swal('Exito!', result.messenger, 'success');
          }
        }, error => {
          console.error(<any>error);
          swal('Error!', error.error.message, 'error');
        });
    }
  }

}
